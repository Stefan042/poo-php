<?php
class Personne {
   private $civilite;
   private $nom;
   private $age;
   
   function __construct($civilite, $nom, $age)
	{
	// Code du constructeur
	
	$this->civilite = $civilite ;
	$this->nom = $nom;
	$this->age=$age;
	
	}

    /**
     * @return mixed
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * @param mixed $civilite
     */
    public function setCivilite($civilite): void
    {
        $this->civilite = $civilite;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }
	
	function afficheTout()
	{
		echo "La civilité est: $this->civilite <BR>";
		echo "Le nom est: $this->nom <BR>";
		echo "L age est: $this->age <BR>" ;

	}

    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return "La civilité est: $this->civilite <BR>".
         "Le nom est: $this->nom <BR>" .
         "L age est: $this->age <BR>" ;
    }


}


class Employe extends Personne {

    private $salaire ;

    /**
     * @param $salaire
     */
    public function __construct($civilite, $nom, $age,$salaire)
    {
        parent::__construct($civilite, $nom, $age) ;
        $this->salaire = $salaire;
    }

    /**
     * @return mixed
     */
    public function getSalaire()
    {
        return $this->salaire;
    }

    /**
     * @param mixed $salaire
     */
    public function setSalaire($salaire): void
    {
        $this->salaire = $salaire;
    }

    function afficheTout()
    {
        parent::afficheTout();
        echo "Et le salair est $this->salaire <BR>";


    }

    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return parent::__toString().
            " et le salaire est".$this->getSalaire() ;
    }

}


$timo = new Personne("Mr","Robert",48) ;
$karia = new Personne("Mr", "Karia", 19 );

echo $timo ;
echo $karia ;

$salarieTimo = new Employe("Mr","Robert",48,2000) ;

// echo $salarieTimo ;
$salarieTimo->afficheTout() ;

?>