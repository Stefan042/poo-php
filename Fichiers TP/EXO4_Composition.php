<?php
class Adresse {
protected $ville;
protected $pays;

public function setVille($ville) {
  $this->ville = $ville;
  }
public function getVille() {
  return $this->ville; }

public function setPays($pays) {
  $this->pays = $pays;
  }
public function getPays() {
  return $this-> pays;
  }



}

class Personne {
protected $nom;
protected $adresse;

  /**
   * @param $nom
   * @param $adresse
   */
  public function __construct($nom)
  {
    $this->nom = $nom;
    $this->adresse = new Adresse();
  }

  /**
   * @return mixed
   */
  public function getAdresse()
  {
    return $this->adresse;
  }

  /**
   * @param mixed $adresse
   */
  public function setAdresse($adresse): void
  {
    $this->adresse = $adresse;
  }



public function setNom($nom) {
    $this->nom = $nom;
  }
public function getNom() {
  return $this->nom;
  }

}

$personne1 = new Personne("Stef");
$personne1->setAdresse("Drancy");

echo $personne1;

?>
