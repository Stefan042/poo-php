<?php
class Compte
{
public $montant;

public function virer($valeur, $destination)
{

    $this->montant -= $valeur;
    $destination->montant += $valeur;

}

    /**
     * @return mixed
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param mixed $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }



}

// Insérez en dessous le code permettant de créer
// une instance de Compte, nommée compteProfesseur

    $compteProfesseur = new Compte();
    /* $compteProfesseur->montant(100); */

    $compteEleve = new Compte();
    /* $compteEleve->montant(100); */

    $compteEleve->virer(50, $compteProfesseur);

    echo $compteProfesseur;

?>
