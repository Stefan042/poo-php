<?php
class Rectangle {
    /**
     * @param int $longueur
     * @param int $largeur
     */
    public function __construct($longueur, $largeur)
    {
        $this->longueur = $longueur;
        $this->largeur = $largeur;
    }

    /**
     * @return int
     */
    public function getLongueur()
    {
        return $this->longueur;
    }

    /**
     * @param int $longueur
     */
    public function setLongueur($longueur)
    {
        $this->longueur = $longueur;
    }

    /**
     * @return int
     */
    public function getLargeur()
    {
        return $this->largeur;
    }

    /**
     * @param int $largeur
     */
    public function setLargeur($largeur)
    {
        $this->largeur = $largeur;
    }



// Declaration des attributs:
private $longueur = 0;
private $largeur = 0;



// Définition de la longueur et largeur du rectangle:
function definitTaille($lg = 0, $la = 0) {
$this->longueur = $lg;
$this->largeur = $la;
}

// Calcule de la surface: longueur * largeur:
function recupereSurface() {
return ($this->longueur * $this->largeur);
}

// Calcul du périmètre: (longueur + largeur) * 2 :
function recuperePerimetre() {
return ( ($this->longueur + $this->largeur) * 2 );
}

// Test pour savoir si le rectangle est un carré
function estCarre() {
if ($this->longueur == $this->largeur) {
return true; // Square
} else {
return false; // Pas un carré
}
}

} // Fin de la classe Rectangle
?>
